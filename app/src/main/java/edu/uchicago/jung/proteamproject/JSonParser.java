package edu.uchicago.jung.proteamproject;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Created by sunginjung on 5/4/17.
 */

public class JSonParser {

    private  JSonParser(){}

    public static ArrayList<Restaurant> fetchRestaurantData(String requestURL){
        URL url = createUrl(requestURL);

        String jsonResponse = null;
        try{
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e("JsonParser", "Error closing input stream", e);
        }

        ArrayList<Restaurant> restaurants = extractRestaurant(jsonResponse);

        return restaurants;

    }

    private static URL createUrl(String stringURL){
        URL url = null;
        try {
            url = new URL(stringURL);
        } catch (MalformedURLException e) {
            Log.e("JsonParder", "Error with creating URL", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e("JsonParser", "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private static ArrayList<Restaurant> extractRestaurant (String JsonResponse) {
        ArrayList<Restaurant> restaurants = new ArrayList<>();

        try{
            JSONObject root = new JSONObject(JsonResponse);
            JSONObject responseObject = root.getJSONObject("response");
            JSONArray venuesArray = responseObject.getJSONArray("venues");

            for (int i = 0; i < venuesArray.length(); i++) {
                JSONObject restaurantJSON = venuesArray.getJSONObject(i);
                String restaurantName = restaurantJSON.getString("name");

                JSONObject contactJSON = restaurantJSON.getJSONObject("contact");
                String phoneNumber = contactJSON.getString("formattedPhone");

                JSONObject locationJSON = restaurantJSON.getJSONObject("location");
                String address = locationJSON.getString("address");
                address = address + ", " + locationJSON.getString("city");
                address = address + ", " + locationJSON.getString("state");

                JSONObject menuJSON = restaurantJSON.getJSONObject("menu");
                String url = menuJSON.getString("mobileUrl");

                restaurants.add(new Restaurant(restaurantName, phoneNumber, address, url ));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return restaurants;
    }
}
