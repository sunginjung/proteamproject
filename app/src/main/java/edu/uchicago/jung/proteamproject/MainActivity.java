package edu.uchicago.jung.proteamproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {

    private EditText mLocationEditText;
    private EditText mCusineEditText;
    private TextView mSearchTextView;

    private String mLocationKeyword;
    private String mCusineKeyword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLocationEditText = (EditText)findViewById(R.id.location_input);
        mCusineEditText = (EditText)findViewById(R.id.cusine_input);
        mSearchTextView = (TextView)findViewById(R.id.search_click);

        mSearchTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location = mLocationEditText.getText().toString().trim();
                String cusine = mCusineEditText.getText().toString().trim();

                if(location.equals("")){
                    Toast.makeText(MainActivity.this, "You need to input both location & cusine!", Toast.LENGTH_SHORT).show();
                } else if (cusine.equals("")){
                    Toast.makeText(MainActivity.this, "You need to input both location & cusine!", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        mLocationKeyword = URLEncoder.encode(location, "UTF-8");
                        mCusineKeyword = URLEncoder.encode(cusine, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Intent listingIntent = new Intent(MainActivity.this, ListingActivity.class);
                    listingIntent.putExtra("location", mLocationKeyword);
                    listingIntent.putExtra("cusine", mCusineKeyword);
                    startActivity(listingIntent);
                }
            }
        });

    }
}
