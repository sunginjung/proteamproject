package edu.uchicago.jung.proteamproject;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

/**
 * Created by sunginjung on 5/4/17.
 */

public class RestaurantLoader extends AsyncTaskLoader<List<Restaurant>> {

    private String mUrl;

    public RestaurantLoader(Context context, String url){
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<Restaurant> loadInBackground() {
        List<Restaurant> restaurants = JSonParser.fetchRestaurantData(mUrl);
        return restaurants;
    }
}
