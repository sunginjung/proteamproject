package edu.uchicago.jung.proteamproject;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListingActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Restaurant>> {

    private RestaurantAdapter mAdapter;
    private String mLocationKeyword;
    private String mCusineKeyword;
    private static final int RESTAURANT_LOADER_ID = 1;
    public static final String QUERY_URL
            = "https://api.foursquare.com/v2/venues/search" +
              "?client_id=G1QNTAESMPPKVVOAJEFNNJSQ0FVMXKZPNDJJTGA5E4ZCGWQV" +
              "&client_secret=2CJI0JQ0X4HLCPUL0ZKK12NYFPDY0MOMGGX3MFF23ANR1FXW" +
              "&v=20130815";
    public static String loaderURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);

        mLocationKeyword = getIntent().getExtras().getString("location");
        mCusineKeyword = getIntent().getExtras().getString("cusine");

        loaderURL = QUERY_URL + "&near=" + mLocationKeyword + "&query=" + mCusineKeyword;

        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(RESTAURANT_LOADER_ID, null, this);

        ListView listView = (ListView)findViewById(R.id.listView);
        mAdapter = new RestaurantAdapter(this, new ArrayList<Restaurant>());
        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Restaurant currentRestaurant = mAdapter.getItem(position);
                Uri uri = Uri.parse(currentRestaurant.getUrl());

                Intent webIntent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(webIntent);
            }
        });
    }

    @Override
    public Loader<List<Restaurant>> onCreateLoader(int id, Bundle args) {
        return new RestaurantLoader(this, loaderURL);
    }

    @Override
    public void onLoadFinished(Loader<List<Restaurant>> loader, List<Restaurant> data) {
        mAdapter.clear();
        if (data != null && !data.isEmpty()) {
            mAdapter.addAll(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Restaurant>> loader) {
        mAdapter.clear();
    }
}
