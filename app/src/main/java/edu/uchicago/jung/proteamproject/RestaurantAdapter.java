package edu.uchicago.jung.proteamproject;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sunginjung on 5/4/17.
 */

public class RestaurantAdapter extends ArrayAdapter<Restaurant>{

    public RestaurantAdapter(Activity context, List<Restaurant> restaurants) {
        super(context, 0, restaurants);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        Restaurant currentRestaurant = getItem(position);
        int backgroundColorID = 0;

        if (position % 2 != 0) {
            backgroundColorID = R.color.colorAccent;
        } else {
            backgroundColorID = R.color.colorPrimaryDark;
        }

        LinearLayout backgroundLayout = (LinearLayout) listItemView.findViewById(R.id.backgorund_list);
        backgroundLayout.setBackgroundColor(backgroundColorID);

        TextView nameTextView = (TextView)listItemView.findViewById(R.id.name);
        nameTextView.setText(currentRestaurant.getName());

        TextView phoneNumberView = (TextView)listItemView.findViewById(R.id.phoneNumber);
        phoneNumberView.setText(currentRestaurant.getPhoneNumber());

        TextView addressView = (TextView)listItemView.findViewById(R.id.address);
        addressView.setText(currentRestaurant.getAddress());

        return listItemView;
    }
}
