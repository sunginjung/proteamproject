package edu.uchicago.jung.proteamproject;

/**
 * Created by sunginjung on 5/4/17.
 */

public class Restaurant {

    private String mName;
    private String mPhoneNumber;
    private String mAddress;
    private String mUrl;

    public Restaurant(String name, String phoneNumber, String address, String url) {
        mName = name;
        mPhoneNumber = phoneNumber;
        mAddress = address;
        mUrl = url;
    }

    public String getName() {
        return mName;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }


    public String getAddress() {
        return mAddress;
    }

    public String getUrl() {return mUrl;}

}
